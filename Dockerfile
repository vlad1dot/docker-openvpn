# syntax=docker/dockerfile:1
FROM alpine:latest

RUN apk add --no-cache iptables openvpn easy-rsa \
    && rm -rf /var/cache/apk/* /var/tmp/* \
    && ln -s /usr/share/easy-rsa/easyrsa /usr/bin/easyrsa

COPY <<EOF /usr/local/bin/openvpn.sh
#!/bin/sh
set -e

/sbin/iptables -I INPUT -i eth0 -m conntrack --ctstate NEW -p udp --dport 1194 -j ACCEPT

/sbin/iptables -I FORWARD -i tun+ -j ACCEPT
/sbin/iptables -I FORWARD -i tun+ -o eth0 -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
/sbin/iptables -I FORWARD -i eth0 -o tun+ -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

/sbin/iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE

/sbin/iptables -A OUTPUT -o tun+ -j ACCEPT

exec /usr/sbin/openvpn --config /etc/openvpn/server/server.conf
EOF

RUN chmod +x /usr/local/bin/openvpn.sh

CMD [ "sh", "-c", "/usr/local/bin/openvpn.sh" ]

# docker builder prune -af \
#     && docker system prune -af \
#     && docker volume prune -f \
#     && docker image prune -af

# docker login
# DOCKER_BUILDKIT=1 docker build --no-cache -t 00x56/docker-openvpn:latest .
# docker push 00x56/docker-openvpn:latest

# docker run --rm --interactive --tty --cap-add=NET_ADMIN --volume $(pwd)/conf/server:/etc/openvpn/server --volume $(pwd)/conf/easy-rsa:/etc/openvpn/easy-rsa --volume $(pwd)/conf/ccd:/etc/openvpn/ccd --volume $(pwd)/log/openvpn-status.log:/var/log/openvpn/openvpn-status.log --workdir /etc/openvpn --device=/dev/net/tun --publish 1194:1194/udp 00x56/docker-openvpn
# docker exec --interactive --tty 7809913082a3 /bin/sh
