# Docker
```
sudo apt update \
    && sudo apt install -y curl \
    && curl -fsSL https://get.docker.com -o get-docker.sh \
    && sudo sh $(pwd)/get-docker.sh
```
# OpenVPN Server
```
sudo apt update \
    && sudo apt install -y openvpn easy-rsa git
```
```
git clone https://gitlab.com/id-0x56/docker-openvpn.git
```
```
cd docker-openvpn \
    && rm -rf $(pwd)/conf/easy-rsa
```
```
cd $(pwd)/conf \
    && make-cadir easy-rsa \
    && cd $(pwd)/easy-rsa
```
```
$(pwd)/easyrsa init-pki \
    && $(pwd)/easyrsa build-ca \
    && $(pwd)/easyrsa gen-crl \
    && $(pwd)/easyrsa gen-dh \
    && /usr/sbin/openvpn --genkey secret $(pwd)/pki/ta.key > /dev/null || /usr/sbin/openvpn --genkey --secret $(pwd)/pki/ta.key > /dev/null
```
```
sudo ln -s /usr/share/easy-rsa/x509-types $(pwd)/pki/x509-types
```
```
$(pwd)/easyrsa gen-req server nopass \
    && $(pwd)/easyrsa sign-req server server
```
```
cd ../..
```
```
sudo docker run --rm --interactive --tty --cap-add=NET_ADMIN --volume $(pwd)/conf/server:/etc/openvpn/server --volume $(pwd)/conf/easy-rsa:/etc/openvpn/easy-rsa --volume $(pwd)/conf/ccd:/etc/openvpn/ccd --volume $(pwd)/log/openvpn-status.log:/var/log/openvpn/openvpn-status.log --workdir /etc/openvpn --device=/dev/net/tun --publish 1194:1194/udp --detach 00x56/docker-openvpn
```
```
sudo docker ps -a
```
# OpenVPN Client
```
cd $(pwd)/conf/easy-rsa
```
```
$(pwd)/easyrsa gen-req user nopass \
    && $(pwd)/easyrsa sign-req client user
```
```
clear && ip addr
```
```
clear && cat $(pwd)/pki/ta.key
```
```
clear && cat $(pwd)/pki/ca.crt
```
```
clear && cat $(pwd)/pki/issued/user.crt
```
```
clear && cat $(pwd)/pki/private/user.key
```
# OpenVPN Revoke
```
cd $(pwd)/conf/easy-rsa
```
```
$(pwd)/easyrsa revoke user \
    && $(pwd)/easyrsa gen-crl
```
